package decorator;

/**
 *
 * @author Edvinas
 */
public abstract class Decorator implements Candy{
    
    protected Candy candy;

    protected Decorator(Candy candy) {
        this.candy = candy;
    }
    
    /**
     *
     * @return
     */
    @Override
    public abstract String getItem();

    /**
     *
     * @return
     */
    @Override
    public abstract double getPrice();

    /**
     *
     * @param money
     * @param price
     * @return
     */
    @Override
    public abstract boolean pay(double money, double price);

    /**
     *
     * @param price
     */
    @Override
    public abstract void setPrice(double price);
    
    /**
     *
     * @param candy
     * @param role
     * @return
     * @throws ClassNotFoundException
     */
    public Decorator getRole(Candy candy, String role) throws ClassNotFoundException{
        if (Class.forName(role).isInstance(candy)){
            if (Class.forName("decorator.SweetCandy").isInstance(this.candy))
                return null;
            return ((Decorator)this.candy).getRole(this.candy, role);
        }
        return (Decorator)candy;
    }
    
    /**
     *
     * @param role
     * @param dec
     * @return
     * @throws ClassNotFoundException
     */
    public Decorator RemoveRole(String role, Decorator dec) throws ClassNotFoundException
    {

        if (Class.forName(role).isInstance(this)){
            return (Decorator)this.candy;
        }
        if (Class.forName("decorator.SweetCandy").isInstance(this.candy)){
            return dec;
        }
        if (Class.forName(role).isInstance(this.candy)){
            this.candy = ((Decorator)this.candy).candy;
        } else if (!(this.candy instanceof SweetCandy)){
            ((Decorator)this.candy).RemoveRole(role, dec);
        }
        return dec;
    }   
    
}
