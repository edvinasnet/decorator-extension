/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package decorator;

/**
 *
 * @author Edvinas
 */
public class MIFCandyShop {
     public static void main(String[] args) throws ClassNotFoundException{
       
         SweetCandy orginal = new SweetCandy();
         System.out.println("Orginl price: "+orginal.getPrice());
         ChangePriceDecorator item = new ChangePriceDecorator(new CandyWithRedPackaging(orginal));
         System.out.println("With red packaging: "+item.getPrice());
         Decorator newItem = item.RemoveRole("decorator.CandyWithRedPackaging", item);
         System.out.println("Without red packaging: "+newItem.getPrice());
     }
}
