package decorator;

/**
 *
 * @author Edvinas
 */
public interface Candy {
    
    public String getItem();
    public double getPrice();
    public void setPrice(double price);
    public boolean pay(double money, double price);
    
}
