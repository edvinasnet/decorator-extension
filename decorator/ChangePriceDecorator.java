/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package decorator;

/**
 *
 * @author Edvinas
 */
public class ChangePriceDecorator extends Decorator{

    public ChangePriceDecorator(Candy candy) {
        super(candy);
    }

    @Override
    public String getItem() {
        return this.candy.getItem();
    }

    @Override
    public double getPrice() {
        return this.candy.getPrice();
    }

    @Override
    public boolean pay(double money, double price) {
        return this.candy.pay(money, price);
    }

    @Override
    public void setPrice(double price) {
        this.candy.setPrice(price);
    }
    
    public void changePrice(int proc){
        double dif = this.candy.getPrice()*proc/100;
        this.candy.setPrice(this.candy.getPrice()+dif);
    }
    
}
