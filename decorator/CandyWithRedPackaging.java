package decorator;

/**
 *
 * @author Edvinas
 */
public class CandyWithRedPackaging extends Decorator{
    
    private String item = "Red Packaging";
    private double price = 0.5;

    public CandyWithRedPackaging(Candy candy) {
        super(candy);
    }

    @Override
    public String getItem() {
        String newItem = this.candy.getItem()+" "+this.item;
        return newItem;
    }

    @Override
    public double getPrice() {
        double newPrice = this.candy.getPrice()+this.price;
        return newPrice;
    }

    @Override
    public boolean pay(double money, double price) {
        return this.candy.pay(money, price);
    }

    @Override
    public void setPrice(double price) {
        this.candy.setPrice(price);
    }
    
}
