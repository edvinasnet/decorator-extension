/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package decorator;

/**
 *
 * @author Edvinas
 */
public class SweetCandy implements Candy{
    private double price = 2;
    private String item = "Sweet candy";
    @Override
    public String getItem() {
        return this.item;
    }

    @Override
    public double getPrice() {
        return this.price;
    }

    @Override
    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public boolean pay(double money, double price) {
        return price <= money;
    }
    
}
