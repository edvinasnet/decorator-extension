package decorator;

/**
 *
 * @author Edvinas
 */
public class ChangeDecorator extends Decorator{

    public ChangeDecorator(Candy candy) {
        super(candy);
    }

    @Override
    public String getItem() {
        return this.candy.getItem();
    }

    @Override
    public double getPrice() {
        return this.candy.getPrice();
    }

    @Override
    public boolean pay(double money, double price) {
        return this.candy.pay(money, price);
    }

    @Override
    public void setPrice(double price) {
        this.candy.setPrice(price);
    }
    
}
