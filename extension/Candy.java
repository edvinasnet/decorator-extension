/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package extension;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Edvinas
 */
public abstract class Candy {
    public Map<String, Extension> extensions = new HashMap<>();
    public abstract double getPrice();
    public abstract void setPrice(double price);
    public abstract String getItem();
    public abstract boolean pay(double money, double price);
    private double price;
    public void addExtensions(String name, Extension extension){
        extensions.put(name, extension);
    }
    
    public void removeExtension(String extension){
        extensions.remove(extension);
    }
    
    public Extension getExtension(String extension){
        return extensions.get(extension);
    }
    
    
}
