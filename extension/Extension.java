/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package extension;

/**
 *
 * @author Edvinas
 */
public interface Extension {
    public String getItem();
    public double getPrice();
    public boolean pay(double money, double price);
}
