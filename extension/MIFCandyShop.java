/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package extension;

/**
 *
 * @author Edvinas
 */
public class MIFCandyShop {
     /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        SweetCandy candy = new SweetCandy();
        candy.addExtensions("changePrice", new ChangePriceExtension(candy));
        System.out.println(candy.getItem()+" PRICE:"+candy.getPrice());
        candy.removeExtension("changePrice");
        System.out.println(candy.getItem()+" PRICE:"+candy.getPrice());

    }
}
