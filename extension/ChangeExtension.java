/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package extension;

/**
 *
 * @author Edvinas
 */
public class ChangeExtension implements Extension{
    private double price = 0.3;
    private String item = "";
    @Override
    public String getItem() {
        return this.item;
    }

    @Override
    public double getPrice() {
        return this.price;
    }

    @Override
    public boolean pay(double money, double price) {
        return price < money;
    }
    
    public double calculateChange(double money, double price)
    {
        return money - price;
    }
    
    
}
