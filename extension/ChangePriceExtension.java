/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package extension;

/**
 *
 * @author Edvinas
 */
public class ChangePriceExtension implements Extension{
    private double price = 1.2;
    private String item = "Extensions";
    private Candy candy;

    public ChangePriceExtension(Candy candy) {
        this.candy = candy;
  
    }
    
    @Override
    public String getItem() {
        return this.item;
    }

    @Override
    public double getPrice() {
        return this.price;
    }

    @Override
    public boolean pay(double money, double price) {
        return price<=money;
    }
    public void changePrice (double discount){
        double price = this.candy.getPrice()-discount;
        this.candy.setPrice(price);
    }
    
}
