/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package extension;

import java.util.Map;
import java.util.Map.Entry;
import static jdk.nashorn.internal.objects.NativeArray.map;

/**
 *
 * @author Edvinas
 */
public class SweetCandy extends Candy{
    private double price = 1;
    private String candy = "Sweet Super Candy";
    @Override
    public double getPrice() {
        double price = 0;
        for (Map.Entry<String, Extension> entry : this.extensions.entrySet())
        {
            price += entry.getValue().getPrice();
        }
        
        return this.price+price;

    }

    @Override
    public String getItem() {
        String name = new String();
 
        for (Map.Entry<String, Extension> entry : this.extensions.entrySet())
        {
            name += entry.getValue().getItem();
            
        }
        return this.candy+ " " + name;
    }

    @Override
    public boolean pay(double money, double price) {
        return price <= money;
    }

    @Override
    public void setPrice(double price) {
        this.price = price;
    }
    
}
